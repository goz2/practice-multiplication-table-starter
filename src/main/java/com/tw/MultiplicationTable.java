package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)) {
            return generateTable(start, end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number > 0 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        var ans = new StringBuilder();
        for (int i = start; i <= end; i++) {
            ans.append(generateLine(start, i));
            if (i != end) {
                ans.append(String.format("%n"));
            }
        }
        return ans.toString();
    }

    public String generateLine(int start, int row) {
        var ans = new StringBuilder();
        for (int i = start; i <= row; i++) {
            ans.append(this.generateExpression(i, row));
            if (i != row) {
                ans.append("  ");
            }
        }
        return ans.toString();
    }

    public String generateExpression(int multiplicand, int multiplier) {
        int result = multiplicand * multiplier;
        String expression = multiplicand + "*" + multiplier + "=" + result;
        return expression;
    }
}
